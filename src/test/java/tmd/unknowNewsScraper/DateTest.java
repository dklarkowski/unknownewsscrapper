package tmd.unknowNewsScraper;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DateTest {

    @Test
    public void test(){
        //given
        String s = "2018-09-17T15:06:28+02:00";

        //when
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH':'mm':'sszzz");
        LocalDateTime dt = LocalDateTime.parse(s, dateTimeFormatter);

        //then
        Assert.assertTrue(true);
    }
}
