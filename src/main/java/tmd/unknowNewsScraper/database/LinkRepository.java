package tmd.unknowNewsScraper.database;

import org.springframework.data.jpa.repository.JpaRepository;
import tmd.unknowNewsScraper.model.Link;
import tmd.unknowNewsScraper.model.News;

import java.util.Optional;
import java.util.Set;

public interface LinkRepository extends JpaRepository<Link, Long> {
}
