package tmd.unknowNewsScraper.database;

import org.springframework.data.jpa.repository.JpaRepository;
import tmd.unknowNewsScraper.model.News;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

public interface NewsRepository extends JpaRepository<News, Long> {

    Optional<News> findByDate(LocalDateTime date);
}
