package tmd.unknowNewsScraper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tmd.unknowNewsScraper.database.LinkRepository;
import tmd.unknowNewsScraper.model.Link;

@Service
public class LinkService {

    @Autowired
    LinkRepository linkRepository;

    public void delete(Link link){
        linkRepository.delete(link);
    }
}
