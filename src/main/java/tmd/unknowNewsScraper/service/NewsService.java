package tmd.unknowNewsScraper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tmd.unknowNewsScraper.database.NewsRepository;
import tmd.unknowNewsScraper.model.News;

import java.util.List;

@Service
public class NewsService {

    @Autowired
    NewsRepository newsRepository;

    public List<News> findAll(){
        return  newsRepository.findAll();
    }

    public News save(News news){
        return newsRepository.save(news);
    }

    public int removeEmptyNews(){
        int i = 0;
        for(News news : findAll()){
            if(news.getLinks().size() == 0){
                newsRepository.delete(news);
                i++;
            }
        }
        return i;
    }
}
