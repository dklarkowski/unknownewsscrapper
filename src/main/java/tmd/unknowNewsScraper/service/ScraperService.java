package tmd.unknowNewsScraper.service;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tmd.unknowNewsScraper.database.NewsRepository;
import tmd.unknowNewsScraper.model.Link;
import tmd.unknowNewsScraper.model.News;

import java.io.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Service
public class ScraperService {

    @Value("${unknowNews.split.regex}")
    String splitingRegex;

    @Value("${unknowNews.path.web-driver}")
    String webdriverLocation;

    @Autowired
    NewsRepository newsRepository;

    //Replace checking by quote with time when time parsing will be done
    private boolean isNewNews(Element element) throws ParseException{

        LocalDateTime date = convertStringToDateTime(element.select("time").attr("datetime"));

        if(!newsRepository.findByDate(date).isPresent()
                && element.text().contains("◢ #unknownews ◣")
                && !element.select("cite").text().isEmpty()
                ){
            return true;
        }
        return false;
    }

    private void parseNews(Document document) throws ParseException {
        Elements elements = document.select("#itemsStream > li > div.wblock");

        for(Element element : elements){
            if(isNewNews(element)){
                News news = new News();
                news.setQuote(element.select("cite").text());
                news.setLinks(parseLinks(element, news));
                news.setNotParsedContent(element.html());
                LocalDateTime date = convertStringToDateTime(element.select("time").attr("datetime"));
                news.setDate(date);
                newsRepository.save(news);
            }
        }
    }

    private LocalDateTime convertStringToDateTime(String s){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH':'mm':'sszzz");
        return LocalDateTime.parse(s, dateTimeFormatter);
    }

    private WebDriver initWebDriver(){
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        System.setProperty("webdriver.chrome.driver", webdriverLocation);
        WebDriver webDriver = new ChromeDriver(chromeOptions);

        return webDriver;
    }

    private Document getPageWithAdditionalLoading() throws InterruptedException {
        String url = "https://www.wykop.pl/tag/unknownews/";

        WebDriver webDriver = initWebDriver();

        JavascriptExecutor jse = (JavascriptExecutor)webDriver;
        webDriver.get(url);

        for(int i=0; i<10; i++){
            jse.executeScript("window.scrollBy(0,document.body.scrollHeight)");
            Thread.sleep(1000);
        }

        return Jsoup.parse(webDriver.getPageSource());
    }


    private Document getPage(){
        String url = "https://www.wykop.pl/tag/unknownews/";

        WebDriver webDriver = initWebDriver();
        webDriver.get(url);

        return Jsoup.parse(webDriver.getPageSource());
    }

    public void saveAllNewNews() throws InterruptedException, ParseException {

        Document document = getPageWithAdditionalLoading();
        parseNews(document);
    }

    @Scheduled(cron = "0 0 * * * *")
    public void saveNewNews() throws ParseException{

        Document document = getPage();
        parseNews(document);
    }

    public List<Link> parseLinks(Element element, News news){

        Element tmpElement = element.select("div.text").first();

        List<Link> tmpList = news.getLinks();

        String[] splitted = tmpElement.text().split("\\ \\d{1,2}\\)\\ ");

        for(String records : splitted){
            if(records.contains("◢ #unknownews ◣")){
                continue;
            }

            String[] splittedRecords = records.split(splitingRegex);
            if(splittedRecords.length < 2){
                continue;
            }
            Link link = new Link();
            link.setNews(news);
            link.setTitle(splittedRecords[0]);
            link.setLink("http://" + splittedRecords[1]);
            if(splittedRecords.length >= 3 && !splittedRecords[2].contains("*******") && !splittedRecords[2].contains("[Chcesz być wołany?]")){
                link.setAdditionalInfo(splittedRecords[2]);
            }
            tmpList.add(link);
        }

        return tmpList;
    }
}
