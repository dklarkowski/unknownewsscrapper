package tmd.unknowNewsScraper.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.net.ntp.TimeStamp;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Entity
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime date;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String quote;

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL)
    private List<Link> links = new ArrayList<>();

    @JsonIgnore
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String notParsedContent;

    public News() {
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getNotParsedContent() {
        return notParsedContent;
    }

    public void setNotParsedContent(String notParsedContent) {
        this.notParsedContent = notParsedContent;
    }
}
