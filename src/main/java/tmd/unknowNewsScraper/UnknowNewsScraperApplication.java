package tmd.unknowNewsScraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UnknowNewsScraperApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnknowNewsScraperApplication.class, args);
	}
}
