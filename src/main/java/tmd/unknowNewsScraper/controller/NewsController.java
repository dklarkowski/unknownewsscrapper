package tmd.unknowNewsScraper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tmd.unknowNewsScraper.database.LinkRepository;
import tmd.unknowNewsScraper.database.NewsRepository;
import tmd.unknowNewsScraper.model.Link;
import tmd.unknowNewsScraper.model.News;
import tmd.unknowNewsScraper.model.Result;
import tmd.unknowNewsScraper.service.NewsService;
import tmd.unknowNewsScraper.service.ScraperService;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class NewsController {

    @Autowired
    ScraperService scraperService;

    @Autowired
    NewsService newsService;

    @GetMapping("/getAllNews")
    public List<News> getAllNews(){
        return newsService.findAll();
    }

    @GetMapping("/saveAll")
    public List<News> saveAllNews() throws InterruptedException, ParseException{
        scraperService.saveAllNewNews();

        return newsService.findAll();
    }

    @GetMapping("/saveNew")
    public List<News> saveNewNews() throws ParseException {
        scraperService.saveNewNews();

        return newsService.findAll();
    }

    @DeleteMapping("/removeAllEmptyNews")
    public Result removeEmptyNews(){

        int removedCount = newsService.removeEmptyNews();

        if(removedCount == 0){
            return new Result("failed", "no empty news in database");
        }

        return new Result("success", removedCount + " news removed");
    }

}
