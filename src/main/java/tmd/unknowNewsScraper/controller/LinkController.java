package tmd.unknowNewsScraper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tmd.unknowNewsScraper.database.LinkRepository;
import tmd.unknowNewsScraper.database.NewsRepository;
import tmd.unknowNewsScraper.model.Link;
import tmd.unknowNewsScraper.model.News;
import tmd.unknowNewsScraper.model.Result;
import tmd.unknowNewsScraper.service.LinkService;

import java.util.Optional;

@RestController
public class LinkController {

    @Autowired
    LinkService linkService;

    @DeleteMapping("/removeLink/{linkId}")
    public Result removeLink(@PathVariable("linkId") Link link){

        linkService.delete(link);

        return new Result("success","link with id: " + link.getId() + " removed");
    }
}
